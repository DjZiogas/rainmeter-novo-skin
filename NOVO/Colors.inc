[Variables]
MainColor=ffffff
MainColorRGB=255,255,255
SubColor=CCCCCC
SubColorRGB=204,204,204
KeyColor=ff2211
KeyColorRGB=255,34,17
WHDD=#ROOTCONFIGPATH#/Images/Drive_Windows.png
HDD=#ROOTCONFIGPATH#/Images/HardDrive.png
HDDOFF=#ROOTCONFIGPATH#/Images/HardDriveOff.png
NHDDON=#ROOTCONFIGPATH#/Images/NetworkDrive_On.png
NHDDOFF=#ROOTCONFIGPATH#/Images/NetworkDrive_Off.png
USB=#ROOTCONFIGPATH#/Images/usb.png
USBX=#ROOTCONFIGPATH#/Images/usbx.png
USBBG=#ROOTCONFIGPATH#/Images/usb_bg_198x32_px.png
USBFG=#ROOTCONFIGPATH#/Images/usb_bars_map.png
RAM=#ROOTCONFIGPATH#/Images/ram.png
CPU=#ROOTCONFIGPATH#/Images/processor.png
MUSIC=#ROOTCONFIGPATH#/Images/music.png
PICS=#ROOTCONFIGPATH#/Images/pictures.png
DOCS=#ROOTCONFIGPATH#/Images/documents.png
DOWN=#ROOTCONFIGPATH#/Images/downloads.png
NET=#ROOTCONFIGPATH#/Images/network.png
GAMES=#ROOTCONFIGPATH#/Images/games.png
COMP=#ROOTCONFIGPATH#/Images/computer.png
CD=#ROOTCONFIGPATH#/Images/CDRom.png
DVD=#ROOTCONFIGPATH#/Images/DVDRom.png
INT=#ROOTCONFIGPATH#/Images/internet.png
HEXAGON=#ROOTCONFIGPATH#/Images/120x101r01.png
NULL=0
ImageA=200



[MainFont]
Meter=String
FontFace=Calibri Light
FontColor=#MainColor#aa
FontSize=24
AntiAlias=1