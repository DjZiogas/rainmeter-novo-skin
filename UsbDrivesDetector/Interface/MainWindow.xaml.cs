﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UsbDetectors;
using INI;

namespace Interface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int DBT_DEVICEARRIVAL = 0x8000;
        private const int DBT_DEVICEREMOVECOMPLETE = 0x8004;
        private const int WM_DEVICECHANGE = 0x0219;
        private readonly string inifile = Environment.CurrentDirectory+"\\usb_settings.txt";

        private SkinManager _skinManager;
        public MainWindow()
        {
            InitializeComponent();
            Ok.IsEnabled = false;
            Cancel.IsEnabled = false;
            if (File.Exists(inifile))
            {
                ReadSettings();
                if (File.Exists(Rainmeter.Text))
                {
                    if (System.IO.Path.GetFileName(Rainmeter.Text) == "Rainmeter.exe")
                    {
                        Ok.IsEnabled = true;
                        Cancel.IsEnabled = true;
                    }
                }
            }
            else
            {
                var file = File.Create(inifile);
                file.Close();
                WriteDefault();
                this.Show();
            }

            Apply.IsEnabled = false;
        }

        private void WriteDefault()
        {
            IniFile file = new IniFile(inifile);
            file.IniWriteValue("Settings","X","0");
            file.IniWriteValue("Settings","Y","0");
            file.IniWriteValue("Settings","GapX","0");
            file.IniWriteValue("Settings","GapY","0");
            file.IniWriteValue("Settings","Rainmeter","");
            file.IniWriteValue("Settings","Skin","");
            ReadSettings();
        }

        private void ReadSettings()
        {
            IniFile file = new IniFile(inifile);
            X.Text = file.IniReadValue("Settings", "X");
            Y.Text = file.IniReadValue("Settings", "Y");
            GapX.Text = file.IniReadValue("Settings", "GapX");
            GapY.Text = file.IniReadValue("Settings", "GapY");
            Rainmeter.Text = file.IniReadValue("Settings", "Rainmeter");
            Skin.Text = file.IniReadValue("Settings", "Skin");
            _skinManager = new SkinManager(
                    Int32.Parse(X.Text),
                    Int32.Parse(Y.Text),
                    Int32.Parse(GapX.Text),
                    Int32.Parse(GapY.Text),
                    Skin.Text,
                    Rainmeter.Text
                );
            _skinManager.UpdateSkins();
        }

        private void SaveSettings()
        {
            IniFile file = new IniFile(inifile);
            file.IniWriteValue("Settings", "X", X.Text);
            file.IniWriteValue("Settings", "Y", Y.Text);
            file.IniWriteValue("Settings", "GapX", GapX.Text);
            file.IniWriteValue("Settings", "GapY", GapY.Text);
            file.IniWriteValue("Settings", "Rainmeter", Rainmeter.Text);
            file.IniWriteValue("Settings", "Skin", Skin.Text);
            _skinManager = new SkinManager(
                    Int32.Parse(X.Text),
                    Int32.Parse(Y.Text),
                    Int32.Parse(GapX.Text),
                    Int32.Parse(GapY.Text),
                    Skin.Text,
                    Rainmeter.Text
                );
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            HwndSource source = PresentationSource.FromVisual(this) as HwndSource;
            source.AddHook(WndProc);
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == WM_DEVICECHANGE)
            {
                if (wParam.ToInt32() == DBT_DEVICEARRIVAL)
                {
                    //TextForUsb.Text = "inserted";
                    _skinManager.UpdateSkins();

                }
                if (wParam.ToInt32() == DBT_DEVICEREMOVECOMPLETE)
                {
                    //TextForUsb.Text = "removed";
                    _skinManager.UpdateSkins();
                }
            }

            return IntPtr.Zero;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            ReadSettings();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SaveSettings();
            Apply.IsEnabled = false;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            SaveSettings();
            _skinManager.UpdateSkins();
            this.Hide();
            Apply.IsEnabled = false;
        }

        private void X_TextChanged(object sender, TextChangedEventArgs e)
        {
            Apply.IsEnabled = true;
        }

        private void Rainmeter_TextChanged(object sender, TextChangedEventArgs e)
        {
            Apply.IsEnabled = true;
            TextBox box = (TextBox)sender;
            if (File.Exists(box.Text))
            {
                if (System.IO.Path.GetFileName(box.Text) == "Rainmeter.exe")
                {
                    Ok.IsEnabled = true;
                    Cancel.IsEnabled = true;
                }
            }
            else
            {
                Ok.IsEnabled = false;
                Cancel.IsEnabled = false;
            }
        }
    }
}
