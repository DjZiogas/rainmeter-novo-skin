﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsbDetectors
{
    public class SkinManager
    {
        private int _posX;
        private int _posY;
        private int _gapY;
        private int _gapX;
        private string _skinPath;
        private string _rainmeterPath;

        public void SetPos(int x, int y)
        {
            _posX = x;
            _posY = y;
        }

        public void SetGaps(int gapx, int gapy)
        {
            _gapX = gapx;
            _gapY = gapy;
        }

        private List<string> _connectedUsbs;
        public SkinManager(string skinPath, string rainmeterPath)
        {
            _skinPath = skinPath;
            _rainmeterPath = rainmeterPath;
            _connectedUsbs = new List<string>();
        }

        public SkinManager(int posX, int posY, int gapX, int gapY, string skinPath, string rainmeterPath)
        {
            _posX = posX;
            _posY = posY;
            _gapX = gapX;
            _gapY = gapY;
            _skinPath = skinPath;
            _rainmeterPath = rainmeterPath;
            _connectedUsbs = new List<string>();
        }

        public void UpdateSkins()
        {
            var oldUsbs = _connectedUsbs;
            _connectedUsbs =
                DriveInfo.GetDrives()
                    .Where(info => info.DriveType == DriveType.Removable && info.IsReady)
                    .Select(info => info.Name)
                    .Take(4)
                    .ToList();
            //find added usbs
            foreach (var connectedUsb in _connectedUsbs)
            {
                LoadSkin(connectedUsb.First().ToString());
            }
            //find removed usbs
            foreach (var oldUsb in oldUsbs)
            {
                if (!_connectedUsbs.Contains(oldUsb))
                {
                    UnloadSkin(oldUsb.First().ToString());
                }
            }

            //relocate all skins
            var lastPosY = _posY;
            var lastPosX = _posX;
            foreach (var connectedUsb in _connectedUsbs)
            {
                MoveSkin(connectedUsb.First().ToString(), lastPosX, lastPosY);
                lastPosX += _gapX;
                lastPosY += _gapY;
            }
        }

        private void UnloadSkin(string drive)
        {
            Process.Start(_rainmeterPath, "!DeactivateConfig " + _skinPath + "\\" + drive.ToUpper());
        }

        private void LoadSkin(string drive)
        {
            Process.Start(_rainmeterPath, "!ActivateConfig " + _skinPath + "\\" + drive.ToUpper());
        }

        private void MoveSkin(string drive, int x, int y)
        {
            var args = "!Move " + x + " " + y + " " + _skinPath + "\\" + drive.ToUpper();
            Process.Start(_rainmeterPath, args);
        }
    }
}
