# Rainmeter NOVO Skin#

This is a skin I created for my personal use.

![Full Desktop view](https://bitbucket.org/repo/R4ybKx/images/218916095-Screenshot_1.png)

Some Icons that i used art taken from **Senary Icons pack by arrioch**

### Usb detection tool ###

This tool alows me to load skin parts for usb drives when they are connected

![Screenshot_2.png](https://bitbucket.org/repo/R4ybKx/images/1961951804-Screenshot_2.png)